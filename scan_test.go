package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var tokens = []string{"addAutoNumberedRecord", "compareGetDataImplementations", "convertIntsToStrings", "countLogs", "createDAG", "createPassthruForm", "createProject", "createQuery", "createTempDir", "createTempFile", "dataDictionaryCSVToMetadataArray", "delayModuleExecution", "deleteDAG", "disableModule", "enableModule", "escape", "exitAfterHook", "getChoiceLabel", "getChoiceLabels", "getConfig", "getCSRFToken", "getDAG", "getData", "getDataTable", "getEnabledModules", "getEventId", "getFieldLabel", "getFieldNames", "getFormsForEventId", "getJavascriptModuleObjectName", "getModuleDirectoryName", "getModuleName", "getModulePath", "getProject", "getProjectId", "getProjectsWithModuleEnabled", "getProjectSetting", "getProjectSettings", "getProjectStatus", "getPublicSurveyHash", "getPublicSurveyUrl", "getQueryLogsSql", "getRecordId", "getRecordIdField", "getRepeatingForms", "getSafePath", "getSettingConfig", "getSettingKeyPrefix", "getSubSettings", "getSystemSetting", "getUrl", "getUser", "getUserSetting", "importDataDictionary", "initializeJavascriptModuleObject", "isAuthenticated", "isModuleEnabled", "isModulePage", "isPage", "isREDCapPage", "isRoute", "isSuperUser", "isSurveyPage", "isValidProjectId", "log", "query", "queryLogs", "records->lock", "redirectAfterHook", "removeLogs", "removeProjectSetting", "removeSystemSetting", "removeUserSetting", "renameDAG", "requireInteger", "resetSurveyAndGetCodes", "sanitizeAPIToken", "sanitizeFieldName", "setDAG", "setProjectId", "setProjectSetting", "setProjectSettings", "getSurveyResponses", "setSystemSetting", "setUserSetting", "throttle", "tt", "tt_addToJavascriptModuleObject", "tt_transferToJavascriptModuleObject", "validateSettings", "validateS3URL", "hasPermission", "saveFile", "saveMetadata", "setData", "addRole", "addUser", "addOrUpdateInstances", "getField", "getForm", "getFormForField", "getLogTable", "getProjectId", "getRepeatingForms", "getRights", "getTitle", "getUsers", "queryData", "removeRole", "removeUser", "setRights", "setRoleForUser", "getFieldNames", "getType", "getUsername", "getEmail", "getRights", "hasDesignRights", "isSuperUser", "afterRender", "ajax", "getUrl", "getUrlParameter", "getUrlParameters", "isImportPage", "isImportReviewPage", "isImportSuccessPage", "isRoute", "log", "tt", "tt_add", "addFileToField", "addFileToRepository", "allowProjects", "allowUsers", "copyFile", "deleteRecord", "email", "escapeHtml", "evaluateLogic", "filterHtml", "getCopyright", "getDataDictionary", "getDataTable", "getEventIdFromUniqueEvent", "getEventNames", "getExportFieldNames", "getFieldNames", "getFieldType", "getFile", "getGroupNames", "getInstrumentNames", "getLogEventTable", "getPDF", "getParticipantEmail", "getParticipantList", "getProjectTitle", "getProjectXML", "getRecordIdField", "getReport", "getSurveyLink", "getSurveyQueueLink", "getSurveyReturnCode", "getUserRights", "getValidFieldsByEvents", "isLongitudinal", "logEvent", "renameRecord", "reserveNewRecordId", "saveData", "storeFile", "versionCompare"}

func TestScan(t *testing.T) {
	// extract files if target folder is missing
	if _, err := os.Stat(test_module); os.IsNotExist(err) {
		DebugLogger.Println("Extracting files")
		err := Unzip("test_data/admin_dash_v3.5.zip", "test_data")
		if err != nil {
			ErrorLogger.Print(err)
		}
	}

	// find all php files in a module directory
	files, err := FindPhpFiles(test_module)
	if err != nil {
		ErrorLogger.Print(err)
	}

	assert.Greater(t, len(files), 0)

	// scan all php files
	for _, file := range files {
		res, err := ScanFile(file, tokens) // Discard the result of ScanFile

		assert.Nil(t, err)
		assert.NotNil(t, res)
		//assert.Greater(t, len(res), 0)
		//DebugLogger.Println("Scanned", res)
	}
}
