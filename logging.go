package main

import (
	"io"
	"log"
	"os"
)

type LogLevelType int64

const (
	None    LogLevelType = 0
	Info    LogLevelType = 1
	Error   LogLevelType = 2
	Warning LogLevelType = 3
	Debug   LogLevelType = 4
)

var (
	WarningLogger *log.Logger
	InfoLogger    *log.Logger
	ErrorLogger   *log.Logger
	DebugLogger   *log.Logger
	LogLevel      LogLevelType = None // 0 = none, 1 = error, 2 = warning, 3 = info, 4 = debug
)

func setLogLevel() {
	logLevel := os.Getenv("LOGLEVEL")
	switch logLevel {
	case "1":
		LogLevel = Info
	case "INFO":
		LogLevel = Info
	case "2":
		LogLevel = Error
	case "ERROR":
		LogLevel = Error
	case "3":
		LogLevel = Warning
	case "WARNING":
		LogLevel = Warning
	case "4":
		LogLevel = Debug
	case "DEBUG":
		LogLevel = Debug
	}
}

func initLoggers() {
	var file io.Writer = nil
	var err error

	if LogLevel > None {
		file, err = os.OpenFile("module-analyzer.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}
	}

	// discard the log output if LogLevel is 0
	if LogLevel == None {
		file = io.Discard
	}

	if LogLevel >= Info {
		InfoLogger = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		InfoLogger = log.New(io.Discard, "", 0)
	}

	if LogLevel >= Warning {
		WarningLogger = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		WarningLogger = log.New(io.Discard, "", 0)
	}

	if LogLevel >= Error {
		ErrorLogger = log.New(file, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		ErrorLogger = log.New(io.Discard, "", 0)
	}

	if LogLevel >= Debug {
		DebugLogger = log.New(file, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		DebugLogger = log.New(io.Discard, "", 0)
	}
}
