package main

import (
	"os"
)

func FindPhpFiles(baseFolder string) ([]string, error) {
	DebugLogger.Println("FindPhpFiles", baseFolder)
	retFiles := []string{}

	// loop over all files in a directory
	files, err := os.ReadDir(baseFolder)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		fileName := baseFolder + "/" + file.Name()

		if file.IsDir() {
			ret, err := FindPhpFiles(fileName)
			if err != nil {
				return nil, err
			}

			retFiles = append(retFiles, ret...)
		} else {
			// skip all files that do not end in ".php"
			if len(fileName) < 4 || fileName[len(fileName)-4:] != ".php" {
				continue
			}
			//fmt.Println("file", fileName)
			retFiles = append(retFiles, fileName)
		}
	}

	return retFiles, nil
}
