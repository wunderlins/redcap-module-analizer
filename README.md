# Module Analizer

This program generates statistics of how many times specific functions are used in redcap modules.
As input a list of functions is expected in the form of an `.xlsx` file (format is described below).

Output will be a list of modules with a count of functions contained in it.

## input file

### Columns

- 0: (optional) id
- **method**: (string) method name (case sensitive)
- min_version: (int, optional) first appearance in redcap
- description: (string, optional)
- deprecated: (bool, optional) 0/1 
- object: (string, optional) in which scope the function works
- **criticality**: (int) 1-3, uncritical, medium, critical

an example can be found in `./redcap_methods.xlsx`.

## Execution

```bash
./module-analizer-arch ./redcap_methods.xlsx ../htdocs/modules \
    > ../module-functions.csv
```

- Parameter 1: path to input file, structure is described above
- Parameter 2: path tothe redcap modules folder
- Output: contains a matrix of modules/functions and the count of each combination

## Build

For release builds, it is recommended to install `make` or `gmake`. You may use 
`go build`/`go test` and `go run` directly during development.

### Prerequisites

- go version 1.22.2 or later
- sed
- (optional) make / gmake

For release builds `$HOME/go` must be in your `$PATH` environment variable

```bash
export PATH=$PATH:$HOME/go
```

### Compile

```bash
make build
make test
```

### Release

```bash
make release
```

This will build for multiple targets. Binaries will be in the project folder
named `module-analizer-<platform>-<arch>`.

