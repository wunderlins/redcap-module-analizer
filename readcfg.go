package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/xuri/excelize/v2"
)

type RedcapMethod struct {
	name        string
	criticality int
}

func ReadFnListXlsx(file string) ([]RedcapMethod, error) {
	f, err := excelize.OpenFile(file)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer func() {
		// Close the spreadsheet.
		if err := f.Close(); err != nil {
			fmt.Println(err)
			return
		}
	}()

	// array of RedcapMethod
	var redcap_methods []RedcapMethod

	// Get all the rows in the Sheet1.
	rows, err := f.GetRows("Sheet1")
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	// loop over lines, skip header
	for _, row := range rows[1:] {
		criticality := 0
		if len(row) > 5 {
			criticality, err = strconv.Atoi(row[6]) // Convert the string value to an integer
			if err != nil {
				fmt.Println(err)
				criticality = -1
			}
		}

		// remove everything after '(' from the name
		name := strings.Split(row[1], "(")[0]
		// trim the name
		name = strings.TrimSpace(name)

		v := RedcapMethod{name: name, criticality: criticality}
		redcap_methods = append(redcap_methods, v)
	}

	return redcap_methods, nil

}
