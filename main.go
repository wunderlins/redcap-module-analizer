package main

import (
	"fmt"
	"os"
	"path"
)

// version is popiilated by the build system with
// go build -ldflags "-X main.version=1.0.0 -X main.date=2021-09-01_00:00:00 -X main.githash=1234567"
var (
	version string
	date    string
	githash string
)

func init() {
	// read loglevel from the environment variable LOGLEVEL
	setLogLevel()

	// initialize loggers
	initLoggers()
}

func main() {

	// config file is expected to be passed in as first parameter
	if len(os.Args) < 2 {
		usage()
		return
	}

	// read the config file
	configFile := os.Args[1]

	// os.Args[1] contains the path tothe modules folder
	if len(os.Args) < 3 {
		usage()
		return
	}
	modulesFolder := os.Args[2]

	// show version
	InfoLogger.Println("Version:", version, date, githash)

	// read the config file
	fns, err := ReadFnListXlsx(configFile)
	if err != nil {
		fmt.Println(err)
		return
	}

	methods := []string{}
	for _, fn := range fns {
		methods = append(methods, fn.name)
	}

	InfoLogger.Println("Methods:", methods)

	// print csv header
	fmt.Print("module,")
	for _, m := range methods {
		fmt.Print(m, ",")
	}
	fmt.Println("")

	// loop over modules in module folder
	dirs, err := os.ReadDir(modulesFolder)
	if err != nil {
		return
	}

	for _, dir := range dirs {
		if !dir.IsDir() {
			continue
		}

		// get a list of all php files in the module
		files, err := FindPhpFiles(modulesFolder + "/" + dir.Name())
		if err != nil {
			ErrorLogger.Print(err)
			return
		}

		// os

		foundMethods := []ScanResult{}
		// scan all php files
		for _, file := range files {
			res, err := ScanFile(file, methods)
			if err != nil {
				ErrorLogger.Print(err)
				panic(err)
			}

			InfoLogger.Println("Scanned", file, "found", len(res), "methods")
			if len(res) > 0 {
				foundMethods = append(foundMethods, res...)
			}
		}

		// create map from methos list
		methodMap := map[string]int{}
		for _, m := range methods {
			methodMap[m] = 0
		}

		// count the number of times each method was found
		for _, m := range foundMethods {
			methodMap[m.Value]++
		}

		// print the results
		fmt.Print(dir.Name(), ",")
		for _, m := range methods {
			fmt.Print(methodMap[m], ",")
		}
		fmt.Println("")
	}

}

func usage() {
	// print usage to stderr

	fmt.Fprintf(os.Stderr, "Usage: %s <config_file> <modules_folder>\n", path.Base(os.Args[0]))
	fmt.Fprintf(os.Stderr, "\n")
	fmt.Fprintf(os.Stderr, "\tconfig_file: the xlsx file containing the list of methods to search for\n")
	fmt.Fprintf(os.Stderr, "\tmodules_folder: the folder containing the modules to search\n")
	fmt.Fprintf(os.Stderr, "\n")
	fmt.Fprintf(os.Stderr, "The log level can be set using the LOGLEVEL environment variable\n")
	fmt.Fprintf(os.Stderr, "\tLOGLEVEL=1: INFO\n")
	fmt.Fprintf(os.Stderr, "\tLOGLEVEL=2: ERROR\n")
	fmt.Fprintf(os.Stderr, "\tLOGLEVEL=3: WARNING\n")
	fmt.Fprintf(os.Stderr, "\tLOGLEVEL=4: DEBUG\n")
	fmt.Fprintf(os.Stderr, "\n")
}
