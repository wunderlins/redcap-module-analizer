# Build and run application
.PHONY: docker

VERSION = 0.2.0-SNAPSHOT
VERSION_CLEAN = $(subst -SNAPSHOT,,$(VERSION))

VERSION_FLAGS = -X main.version=$(VERSION) \
	-X main.date=$(shell date -u '+%Y-%m-%d_%H:%M:%S') \
	-X main.githash=$(shell git rev-parse --short HEAD)
LDFLAGS = -s -w 

run:
	go run -ldflags '$(VERSION_FLAGS)' .

all: build test

build:
	go build -ldflags '$(VERSION_FLAGS)' .

test:
	go test -ldflags '$(VERSION_FLAGS)' .

coverage: test
	go test -coverprofile=coverage.out
	# remove main function from tests
	sed -i -e '/gitlab.com\/wunderlins\/redcap-module-analizer\/main.go/d' coverage.out
	go tool cover -html=coverage.out

clean:
	rm -rf module-analizer* *.syso coverage.out
	rm -rf redcap-module-analizer test_data/admin_dash_v3.5

release: \
	icon \
	module-analizer-linux-amd64 \
	module-analizer-linux-arm64 \
	module-analizer-windows-amd64.exe \
	module-analizer-darwin-amd64 \
	module-analizer-darwin-arm64

module-analizer-linux-amd64:
	GOOS=linux   GOARCH=amd64 go build -ldflags '$(LDFLAGS) $(VERSION_FLAGS)' -o module-analizer-linux-amd64 .
module-analizer-linux-arm64:
	GOOS=linux   GOARCH=arm64 go build -ldflags '$(LDFLAGS) $(VERSION_FLAGS)' -o module-analizer-linux-arm64 .
module-analizer-windows-amd64.exe:
	GOOS=windows GOARCH=amd64 go build -ldflags '$(LDFLAGS) $(VERSION_FLAGS)' -o module-analizer-windows-amd64.exe .
module-analizer-darwin-amd64:
	GOOS=darwin  GOARCH=amd64 go build -ldflags '$(LDFLAGS) $(VERSION_FLAGS)' -o module-analizer-darwin-amd64 .
module-analizer-darwin-arm64:
	GOOS=darwin  GOARCH=arm64 go build -ldflags '$(LDFLAGS) $(VERSION_FLAGS)' -o module-analizer-darwin-arm64 .

version:
	@echo $(VERSION)

icon:
	@echo export PATH=$$PATH:~/go/bin
	go install github.com/tc-hib/go-winres@latest
	sed -i -e 's/.*"file_version":.*/          "file_version": "$(VERSION_CLEAN).0",/' winres/winres.json
	sed -i -e 's/.*"product_version":.*/          "product_version": "$(VERSION_CLEAN).0"/' winres/winres.json
	sed -i -e 's/.*"ProductVersion":.*/            "ProductVersion": "$(VERSION_CLEAN)",/' winres/winres.json
	sed -i -e 's/.*"FileVersion":.*/            "FileVersion": "$(VERSION_CLEAN)",/' winres/winres.json
	go-winres make