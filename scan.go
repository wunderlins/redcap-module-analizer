package main

import (
	"go/scanner"
	"go/token"
	"os"
)

type ScanResult struct {
	FileName string
	Line     int
	Column   int
	Type     token.Token
	Value    string
}

func IndexOf(arr []string, candidate string) int {
	for index, c := range arr {
		if c == candidate {
			return index
		}
	}
	return -1
}

func ScanFile(fileName string, tokens []string) ([]ScanResult, error) {
	ret := []ScanResult{}

	// open file
	file, err := os.Open(fileName)
	if err != nil {
		ErrorLogger.Println("Error: ", err)
		return nil, err
	}

	defer file.Close()

	// read file into []byte
	fileInfo, _ := file.Stat()
	fileSize := fileInfo.Size()
	buffer := make([]byte, fileSize)

	length, err := file.Read(buffer)
	if err != nil {
		ErrorLogger.Println("Error: ", err)
		return nil, err
	}

	if int64(length) != fileSize {
		ErrorLogger.Println("Error: Couldn't read the whole file")
		return nil, err
	}

	// Initialize the scanner.
	var s scanner.Scanner

	fset := token.NewFileSet() // positions are relative to fset

	// register input "file"
	content := fset.AddFile("", fset.Base(), len(buffer))
	s.Init(content, buffer, nil /* no error handler */, scanner.ScanComments)

	for {
		pos, tok, lit := s.Scan()
		if tok == token.EOF {
			break
		}

		if tok != token.IDENT {
			continue
		}

		// check if toeksn contains lit
		if IndexOf(tokens, lit) == -1 {
			continue
		}

		//DebugLogger.Printf("%s\t%s\t%q\n", fset.Position(pos), tok, lit)
		var result = ScanResult{
			FileName: fileName,
			Line:     fset.Position(pos).Line,
			Column:   fset.Position(pos).Column,
			Type:     tok,
			Value:    lit,
		}
		ret = append(ret, result)
	}

	// return ret
	return ret, nil
}
