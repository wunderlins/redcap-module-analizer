package main

import "testing"

func TestReadFnListXlsx(t *testing.T) {
	// test the ReadFnList function
	redcap_methods, err := ReadFnListXlsx("redcap_methods.xlsx")
	if err != nil {
		t.Error("Error reading file")
	}

	if len(redcap_methods) != 172 {
		t.Error("Expected 172 methods, got ", len(redcap_methods))
	}
}
